import { Component, Input } from '@angular/core';
import { navItems } from './../../_nav';
import {DataService} from '../../data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  avatar: string;
  constructor(private data: DataService,
              private router: Router) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
    // this.data.getCompanyProfile().then(() => {
    //   this.avatar = 'http://it-univer43.catkov.beget.tech/uploads/' + this.data.user.avatar;
    // });
  }

  async logout() {
    await this.data.clearProfile();
    this
      .router
      .navigate(['/login']);
  }
}
