import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import qs from 'qs';
import {environment} from '../environments/environment';

// const API_URL = 'http://46.161.39.213:4367';
const API_URL = 'http://193.176.78.104:1002';

@Injectable({providedIn: 'root'})
export class RestApiService {

  constructor(private http: HttpClient) {
  }

  getHeaders() {
    const token = localStorage.getItem('curator_token');
    return token
      ? new HttpHeaders().set('Authorization', token)
      : null;
  }

  get(link: string) {
    return this
      .http
      .get(link, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  post(link: string, body: any) {
    return this
      .http
      .post(link, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  put(link: string, body: any) {
    return this
      .http
      .put(link, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  loginCurator(body: any) {
    return this
      .http
      .post(`${API_URL}/api/curator/login`, body)
      .toPromise();
  }

  resetCurator(body: any) {
    return this
      .http
      .post(`${API_URL}/api/curator/reset/password`, body)
      .toPromise();
  }

  signupCurator(body: any) {
    return this
      .http
      .post(`${API_URL}/api/curator/signup`, body)
      .toPromise();
  }

  getCourseById(id) {
    return this
      .http
      .get(`${API_URL}/api/course/${id}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseLogo(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/update/logo`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  addPicture(body: any) {
    return this
      .http
      .post(`${API_URL}/api/add/picture`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getPictures() {
    return this
      .http
      .get(`${API_URL}/api/all_pictures`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseLecturerPhoto(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/update/lecturer_photo`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  uploadImage(apiUrl: string, body: any) {
    const headers = this.getHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    console.log(body);
    return this
      .http
      .post(`${apiUrl}/uploads/new.php`, body)
      .toPromise();
  }

  updateCompany(body: any) {
    return this
      .http
      .put(`${API_URL}/api/company/profile/update`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  checkPassword(body: any) {
    return this
      .http
      .post(`${API_URL}/api/curator/check/password`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  uploadVectorImage(apiUrl: string, body: any) {
    const headers = this.getHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    console.log(body);
    return this
      .http
      .post(`${apiUrl}/uploads/new_vector.php`, body)
      .toPromise();
  }

  updateCurator(body: any) {
    return this
      .http
      .put(`${API_URL}/api/curator/profile/update`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updatePassword(body: any) {
    return this
      .http
      .put(`${API_URL}/api/curator/profile/update/password`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getCuratorProfile() {
    return this
      .http
      .get(`${API_URL}/api/curator/profile`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getCourseInstanceById(id) {
    return this
      .http
      .get(`${API_URL}/api/course/instance/${id}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseInstanceById(id, body: any) {
    return this
      .http
      .put(`${API_URL}/api/course/update/instance/${id}`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseInstanceLogoById(body: any) {
    return this
      .http
      .put(`${API_URL}/api/course/update/instance_logo`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourseInstanceLecturerPhotoById(body: any) {
    return this
      .http
      .put(`${API_URL}/api/course/update/instance_lecturer_photo`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  addCourse(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/add`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  addCourseInstance(body: any) {
    return this
      .http
      .post(`${API_URL}/api/course/add/instance`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  updateCourse(body: any) {
    return this
      .http
      .post(`${API_URL}/api/curator/course/update`, body, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  approveCourse(approve: boolean, body: any) {
    if (approve) {
      return this
        .http
        .put(`${API_URL}/api/course/approve`, body, {
          headers: this.getHeaders()
        })
        .toPromise();
    } else {
      return this
        .http
        .put(`${API_URL}/api/course/unapprove`, body, {
          headers: this.getHeaders()
        })
        .toPromise();
    }
  }


  getAllCities() {
    return this
      .http
      .get(`${API_URL}/api/cities`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getAllCompanies() {
    return this
      .http
      .get(`${API_URL}/api/companies`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getCompanyCourses(companyId) {
    return this
      .http
      .get(`${API_URL}/api/course/company/${companyId}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getUniversityCourses(universityId) {
    return this
      .http
      .get(`${API_URL}/api/course/university/${universityId}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getUniversityCourseInstances(universityId) {
    return this
      .http
      .get(`${API_URL}/api/course/instances/university/${universityId}`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }

  getAllUniversities() {
    return this
      .http
      .get(`${API_URL}/api/universities`, {
        headers: this.getHeaders()
      })
      .toPromise();
  }
}
