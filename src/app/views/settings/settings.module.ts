// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SecurityComponent } from './security.component';
import { ProfileComponent } from './profile.component';

// Theme Routing
import { SettingsRoutingModule } from './settings-routing.module';
import {FormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    NgxMaskModule.forRoot(),
    FormsModule
  ],
  declarations: [
    SecurityComponent,
    ProfileComponent
  ]
})
export class SettingsModule { }
