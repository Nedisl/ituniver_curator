import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import { FormsModule } from '@angular/forms'; import { ReactiveFormsModule } from '@angular/forms';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {UploadFileService} from '../../upload-file.service';
import {NgxSpinnerService} from 'ngx-spinner';


@Component({
  templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit {
  universityId: number;
  firstName: string;
  lastName: string;
  fatherName: string;
  email: string;
  phone: string;
  submitDisabled: boolean;

  isEmailValid: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isUniversityCorrect: boolean;
  allCorrect: boolean;

  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private spinner: NgxSpinnerService) {

  }

  async ngOnInit() {
    this.isEmailValid = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isUniversityCorrect = true;
    this.allCorrect = true;
    await this.spinner.show();
    await this.data.getCuratorProfile();
    await this.spinner.hide();
    this.universityId = this.data.user.university_id;
    this.fatherName = this.data.user.father_name;
    this.firstName = this.data.user.first_name;
    this.lastName = this.data.user.last_name;
    this.phone = this.data.user.phone;
    this.email = this.data.user.email;
  }

  async update() {
    this.submitDisabled = true;

    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid = re.test(String(this.email).toLowerCase());
    this.isNameCorrect = (this.firstName !== '' && this.lastName !== '' && this.fatherName !== '');
    this.isPhoneCorrect = (this.phone.length === 10);
    this.isUniversityCorrect = (this.universityId !== 0);
    this.submitDisabled = true;
    this.allCorrect = this.isEmailValid && this.isNameCorrect && this.isPhoneCorrect && this.isUniversityCorrect;

    if (this.allCorrect) {
      try {
        await this
          .rest
          .updateCurator({
            first_name: this.firstName,
            last_name: this.lastName,
            father_name: this.fatherName,
            email: this.email,
            phone: this.phone,
            university_id: this.universityId,
          }).then((res) => {
            console.log(res);
            this
              .data
              .addToast('Данные успешно обновлены', '', 'success');
            this
              .router
              .navigate(['/dashboard']);
          });
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }

      this.submitDisabled = false;
    }
  }
}
