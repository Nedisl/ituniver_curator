import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'security.component.html'
})
export class SecurityComponent implements OnInit {
  password: string;
  passwordRepeat: string;
  passwordOld: string;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {

  }

  async savePassword() {
    if (this.password === this.passwordRepeat) {
      const isOldPasswordOk = await this.rest.checkPassword({
        email: this.data.user.email,
        password: this.passwordOld
      });
      if (isOldPasswordOk['meta'].success) {
        this
          .rest
          .updatePassword({
            password: this.password
          }).then((res) => {
          console.log(res);
          this
            .data
            .addToast('Данные успешно обновлены', '', 'success');
          this
            .router
            .navigate(['/dashboard']);
        });
      } else {
        this
          .data
          .addToast('Вы ввели неверный старый пароль!', '', 'error');
      }
    } else {
      this
        .data
        .addToast('Пароли не свопадают', '', 'error');
    }
  }

  async ngOnInit() {
    await this.spinner.show();
    this.data.getCuratorProfile().then(result => {
      console.log(this.data.user);
      this.spinner.hide();
    });
  }
}
