import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})

// TODO: Fix validation shit code to Rx driven
export class RegisterComponent implements OnInit{
  first_name: string;
  last_name: string;
  father_name: string;
  email: string;
  password: string;
  repeat_password: string;
  university_id: number;
  phone: string;

  isEmailValid: boolean;
  isPasswordMatch: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isUniversityCorrect: boolean;
  allCorrect: boolean;

  submitDisabled = false;
  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    this.isEmailValid = true;
    this.isPasswordMatch = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isUniversityCorrect = true;
    this.allCorrect = true;
    this.first_name = '';
    this.last_name  = '';
    this.father_name = '';
    this.email = '';
    this.password = '';
    this.repeat_password = '';
    this.university_id = 0;
    this.phone = '';
  }

  async register() {
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid =  re.test(String(this.email).toLowerCase());
    this.isPasswordMatch = ((this.password === this.repeat_password) && this.password !== '');
    this.isNameCorrect = (this.first_name !== '' && this.last_name !== '' && this.father_name !== '');
    this.isPhoneCorrect = (this.phone.length === 10);
    this.isUniversityCorrect = (this.university_id !== 0);
    this.submitDisabled = true;
    this.allCorrect = this.isEmailValid && this.isPasswordMatch && this.isNameCorrect && this.isPhoneCorrect && this.isUniversityCorrect;

    if (this.allCorrect) {
      console.log('ok');
      try {
            await this.spinner.show();
            await this
              .rest
              .signupCurator({
                email: this.email,
                password: this.password,
                first_name: this.first_name,
                last_name: this.last_name,
                father_name: this.father_name,
                phone: this.phone,
                university_id: this.university_id
              }).then(async (res) => {
                await this
                  .data
                  .addToast('Вы успешно зарегистрированы', '', 'success');
                await this.spinner.hide();
                await this.login();
              });
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }

      this.submitDisabled = false;
    }
  }

  async login() {
    try {
      const res = await this
        .rest
        .loginCurator({
          email: this.email,
          password: this.password,
        });
      if (res['meta'].success) {
        localStorage.setItem('curator_token', res['data'].token);

        this
          .data
          .addToast('Вы успешно авторизованы', '', 'success');
        this
          .router
          .navigate(['/dashboard']).then(() => {
        });
      } else {
        this
          .data
          .addToast('Неверно введен логин или пароль!', '', 'error');
      }
    } catch (error) {
      this
        .data
        .addToast(error, '', 'error');
    }
  }

}
