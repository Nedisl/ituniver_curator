import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list.component';
import { AddComponent } from './add.component';
import { EditComponent } from './edit.component';
import {OngoingComponent} from './ongoing.component';
import {CourseComponent} from './course.component';
import {NewpictureComponent} from './newpicture.component';
import {PicturesComponent} from './pictures.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Курсы'
    },
    children: [
      {
        path: 'catalog',
        component: ListComponent,
        data: {
          title: 'Каталог'
        }
      },
      {
        path: 'ongoing',
        component: OngoingComponent,
        data: {
          title: 'Текущие'
        }
      },
      {
        path: 'add',
        component: AddComponent,
        data: {
          title: 'Добавить'
        }
      },
      {
        path: 'edit/:id',
        component: EditComponent,
        data: {
          title: 'Редактировать'
        }
      },
      {
        path: 'course/:id',
        component: CourseComponent,
        data: {
          title: 'Курс'
        }
      },
      {
        path: 'course/pictures/add',
        component: NewpictureComponent,
        data: {
          title: 'Добавить изображение'
        }
      },
      {
        path: 'list/pictures',
        component: PicturesComponent,
        data: {
          title: 'Изображения'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule {}
