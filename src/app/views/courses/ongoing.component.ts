import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import * as moment from 'moment';

@Component({
  templateUrl: 'ongoing.component.html'
})

export class OngoingComponent implements OnInit {
  courseInstances: any = null;
  showedInstances: any = null;
  startDate: string;
  endDate: string;
  isDateFilter: boolean;
  isCompanyFilter: boolean;
  isCurrentFilter: boolean;
  courseName: string;
  courseId: number;
  companyId: number;
  allCourses: any = null;
  allCompanies: any = null;

  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    this.isDateFilter = false;
    this.courseName = '';
    this.startDate = '';
    this.endDate = '';
    this.courseId = 0;
    this.companyId = 0;
    this.isCompanyFilter = false;
    this.isCurrentFilter = false;
    await this.spinner.show();
    await this.data.getCuratorProfile();
    if (this.courseInstances == null || this.allCourses == null || this.allCompanies == null) {
      await this.spinner.show();
    }
    await this.data.getCuratorProfile();
    this.allCourses = await this.rest.getUniversityCourses(this.data.user.university_id);
    this.allCompanies = await this.rest.getAllCompanies();
    this.courseInstances = await this.rest.getUniversityCourseInstances(this.data.user.university_id);
    this.showedInstances = await this.courseInstances.data.course_instances;
    await this.spinner.hide();
  }

  async filterInstances() {
    console.log(this.startDate);
    this.showedInstances = await this.courseInstances.data.course_instances;
    if (+this.courseId !== 0) {
      this.showedInstances = await this.filterByName(this.showedInstances, this.courseId);
    }
    if (+this.companyId !== 0) {
      this.showedInstances = await this.filterByCompany(this.showedInstances, this.companyId);
    }
    if (this.startDate !== '' && this.startDate.length === 10) {
      this.showedInstances = await this.filterByStart(this.showedInstances, this.startDate);
    }
    if (this.endDate !== '' && this.endDate.length === 10) {
      this.showedInstances = await this.filterByEnd(this.showedInstances, this.endDate);
    }
    if (this.isCurrentFilter) {
      this.showedInstances = await this.filterIsCurrent(this.showedInstances);
    }

    console.log(this.showedInstances);
  }

  async filterIsCurrent(instances) {
    const filteredInstances = await instances.filter(function (instance) {
      return moment(instance.end_date) >= moment();
    });

    return filteredInstances;
  }

  async filterByCompany(instances, companyId) {
    const filteredInstances = await instances.filter(function (instance) {
      return instance.course.company_id === +companyId;
    });

    return filteredInstances;
  }

  async filterByName(instances, id) {
    const filteredInstances = await instances.filter(function (instance) {
      return instance.course_id === +id;
    });

    return filteredInstances;
  }

  async filterByStart(instances, start) {
    const filteredInstances = await instances.filter(function (instance) {
      return moment(instance.start_date) >= moment(start, 'DD.MM.YYYY');
    });

    return filteredInstances;
  }

  async filterByEnd(instances, end) {
    const filteredInstances = await instances.filter(function (instance) {
      return moment(instance.start_date) <= moment(end, 'DD.MM.YYYY');
    });

    return filteredInstances;
  }
}

