import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import { FormsModule } from '@angular/forms'; import { ReactiveFormsModule } from '@angular/forms';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {DatePipe} from '@angular/common';
import {UploadFileService} from '../../upload-file.service';


@Component({
  templateUrl: 'edit.component.html'
})
export class EditComponent implements OnInit {
  submitDisabled: boolean;
  schedule: string;
  lectorName: string;
  lecturerPhoto: string;
  courseName: string;
  logo: string;
  place: string;
  courseCompany: string;
  startDate: any;
  endDate: any;
  courseId: number;
  private sub: any;
  course: any = null;
  pipe: any;
  endApplyDate: string;
  isStartDateCorrect: boolean;
  isPlaceCorrect: boolean;
  isEndDateCorrect: boolean;
  isImageCorrect: boolean;
  isApplyEndDateCorrect: boolean;
  isScheduleCorrect: boolean;
  allCorrect: boolean;
  pictures: any = null;
  pictureId: number;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private route: ActivatedRoute,
               private fileUploader: UploadFileService) {
    this.pipe = new DatePipe('en-US');
    this.data.getCuratorProfile();
    this.sub = this.route.params.subscribe(params => {
      this.courseId = +params['id'];
      this.rest.getCourseInstanceById(+params['id']).then(course => {
        this.course = course;
        this.schedule = this.course.data.schedule;
        this.courseName = this.course.data.course.name;
        if (this.course.data.course.picture_id !== null) {
          this.pictureId = +this.course.data.picture_id;
        } else {
          this.pictureId = 0;
        }
        this.place = this.course.data.place;
        this.startDate = this.pipe.transform(this.course.data.start_date, 'dd.MM.yyyy');
        this.endDate = this.pipe.transform(this.course.data.end_date, 'dd.MM.yyyy');
        this.endApplyDate = this.pipe.transform(this.course.data.apply_end_date, 'dd.MM.yyyy');
      });
    });

    this.isStartDateCorrect = true;
    this.isEndDateCorrect = true;
    this.isApplyEndDateCorrect = true;
    this.isScheduleCorrect = true;
    this.allCorrect = true;
    this.isPlaceCorrect = true;
  }

  async ngOnInit() {
    this.pictures = await this.rest.getPictures();
  }

  isValidDate(s) {
    const bits = s.split('.');
    const d = new Date(+bits[2],  bits[1] - 1, +bits[0]);
    return (d && (d.getMonth() + 1) === +bits[1]) && (s.length === 10);
  }


  async updateCourse() {
    this.submitDisabled = true;
    this.isImageCorrect = !!this.pictureId;
    this.isEndDateCorrect = this.isValidDate(this.endDate);
    this.isStartDateCorrect = this.isValidDate(this.startDate);
    this.isApplyEndDateCorrect = this.isValidDate(this.endApplyDate);
    this.isScheduleCorrect = (this.schedule !== '');
    this.isPlaceCorrect = (this.place !== '');
    this.allCorrect = this.isImageCorrect && this.isEndDateCorrect && this.isStartDateCorrect && this.isApplyEndDateCorrect && this.isScheduleCorrect && this.isPlaceCorrect;

    if (this.allCorrect) {
      try {
        await this
          .rest
          .updateCourseInstanceById(this.courseId, {
            start_date: this.startDate,
            end_date: this.endDate,
            apply_end_date: this.endApplyDate,
            schedule: this.schedule,
            place: this.place,
            picture_id: this.pictureId
          }).then((res) => {
            console.log(res);
            this
              .data
              .addToast('Курс успешно обновлен!', '', 'success');
            this
              .router
              .navigate(['/courses/ongoing']);
          });
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }
}
