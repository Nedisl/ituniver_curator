import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'list.component.html'
})


export class ListComponent implements OnInit {
  courses: any = null;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    if (this.courses == null) {
      await this.spinner.show();
    }

    await this.data.getCuratorProfile();
    this.courses = await this.rest.getUniversityCourses(this.data.user.university_id);
    await this.courses.data.courses.sort((a, b) => (a.approved > b.approved) ? 1 : ((b.approved > a.approved) ? -1 : 0));
    await this.spinner.hide();
    console.log(this.courses);
  }

  async onApprove(courseId, status) {
    await this.rest.approveCourse(status, {
      course_id: courseId,
      university_id: this.data.user.university_id
    }).then(res => {
      console.log(res);
    });
  }
}

