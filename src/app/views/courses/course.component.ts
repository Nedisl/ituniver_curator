import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../data.service';
import { RestApiService } from '../../rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ngx-toasty';
import { UploadFileService } from '../../upload-file.service';


@Component({
  templateUrl: 'course.component.html'
})
export class CourseComponent implements OnInit {
  @ViewChild('reqName', { static: false }) reqName: ElementRef;
  university_id: number;
  submitDisabled: boolean;
  name: string;
  description: string;
  id: number;
  private sub: any;
  course: any = null;
  volume: number;
  lecturerName: string;
  lecturerPhoto: string;
  lecturerAbout: string;
  isApproved: boolean;
  logo: string;
  isNameCorrect: boolean;
  isDescriptionCorrect: boolean;
  isUniversityCorrect: boolean;
  isLecturerNameCorrect: boolean;
  allCorrect: boolean;
  isLecturerAboutCorrect: boolean;
  isAnyChanges: boolean;
  isTopicValid = true;
  weeks: any;
  requirements: any;

  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private route: ActivatedRoute,
              private fileUploader: UploadFileService) {

  }

  async ngOnInit() {
    this.data.getCuratorProfile();
    this.isAnyChanges = false;
    this.isNameCorrect = true;
    this.isDescriptionCorrect = true;
    this.isUniversityCorrect = true;
    this.isLecturerNameCorrect = true;
    this.allCorrect = true;
    this.lecturerAbout = '';
    this.isLecturerAboutCorrect = true;
    this.volume = 0;

    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.rest.getCourseById(+params['id']).then(course => {
        console.log(course);
        this.course = course;
        this.name = this.course.data.name;
        this.description = this.course.data.description;
        this.university_id = this.course.data.university_id;
        this.logo = 'http://it-univer43.catkov.beget.tech/uploads/' + this.course.data.logo;
        this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/' + this.course.data.lecturer_photo;
        this.lecturerName = this.course.data.lecturer_name;
        this.lecturerAbout = this.course.data.lecturer_about;
        this.isApproved = this.course.data.approved;
        this.volume = +this.course.data.volume;
        if (this.course.data.weeks) {
          this.weeks = JSON.parse(this.course.data.weeks).weeks;
        } else {
          this.weeks = [];
        }
        if (this.course.data.requirements) {
          this.requirements = JSON.parse(this.course.data.requirements).requirements;
        } else {
          this.requirements = [];
        }
      });
    });
  }

  async approveCourse() {
    await this.rest.approveCourse(true, {
      course_id: this.id,
      university_id: this.data.user.university_id
    }).then(res => {
      this.rest.getCourseById(this.id).then(course => {
        this.course = course;
        this.name = this.course.data.name;
        this.description = this.course.data.description;
        this.university_id = this.course.data.university_id;
        this.logo = 'http://it-univer43.catkov.beget.tech/uploads/' + this.course.data.logo;
        this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/' + this.course.data.lecturer_photo;
        this.lecturerName = this.course.data.lecturer_name;
        this.lecturerAbout = this.course.data.lecturer_about;
        this.volume = +this.course.data.volume;
        this.isApproved = this.course.data.approved;
        if (this.course.data.weeks) {
          this.weeks = JSON.parse(this.course.data.weeks).weeks;
        } else {
          this.weeks = [];
        }

        this
          .data
          .addToast('Курс успешно подтвержден!', '', 'success');
        this
          .router
          .navigate(['/courses/catalog']);
      });
    });
  }

  addReq() {
    this.requirements.push({ name: '', deleted: false });
  }

  checkReqs() {
    if (this.requirements.some(e => e.name === '' && e.deleted === false)) {
      return true;
    }

    return false;
  }

  async updateCourse() {
    this.submitDisabled = true;

    this.isLecturerAboutCorrect = (this.lecturerAbout !== '');
    this.isNameCorrect = (this.name !== '');
    this.isDescriptionCorrect = (this.description !== '');
    this.isUniversityCorrect = (this.university_id !== 0);
    this.isLecturerNameCorrect = (this.lecturerName !== '');
    this.allCorrect = this.isNameCorrect && this.isDescriptionCorrect && this.isUniversityCorrect
      && this.isLecturerNameCorrect && this.isLecturerAboutCorrect;

    if (this.allCorrect) {
      try {
        await this
          .rest
          .updateCourse({
            course_id: this.id,
            name: this.name,
            description: this.description,
            university_id: this.university_id,
            lecturer_name: this.lecturerName,
            lecturer_about: this.lecturerAbout,
            volume: this.volume,
            weeks: {
              weeks: this.weeks.filter(function (el) {
                return el.deleted !== true;
              })
            },
            requirements: {
              requirements: this.requirements.filter(function (el) {
                return el.deleted !== true;
              })
            }
          }).then((res) => {
            console.log(res);
            this
              .data
              .addToast('Курс успешно обновлен!', '', 'success');
            this
              .router
              .navigate(['/courses/catalog']);
          });
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

  async logoChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadVectorImage(file);

      try {
        await this
          .rest
          .updateCourseLogo({
            logo: pictureUrl,
            course_id: this.id
          });
        this.logo = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.course.data.logo = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this
          .data
          .success('Логотип обновлен');

        await this.rest.getCourseById(this.id);
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

  addWeek() {
    this.weeks.push({ name: '', deleted: false, topics: [] });
  }

  addTopic(week) {
    week.topics.push({ name: '', deleted: false });
  }

  checkWeeks() {
    if (this.weeks.some(e => e.name === '' && e.deleted === false)) {
      return true;
    }

    return false;
  }

  checkTopics(topics) {
    if (topics.some(e => e.name === '' && e.deleted === false)) {
      this.isTopicValid = false;
      return true;
    }

    this.isTopicValid = true;
    return false;
  }

  async photoChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadImage(file);

      try {
        await this
          .rest
          .updateCourseLecturerPhoto({
            course_id: this.id,
            lecturer_photo: pictureUrl
          });
        this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.course.data.lecturer_photo = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this
          .data
          .success('Фото лектора обновлено');

        await this.rest.getCourseById(this.id);
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }


}
