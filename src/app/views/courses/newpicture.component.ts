import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import { FormsModule } from '@angular/forms'; import { ReactiveFormsModule } from '@angular/forms';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {UploadFileService} from '../../upload-file.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {isValidDate} from 'ngx-bootstrap/timepicker/timepicker.utils';


@Component({
  templateUrl: 'newpicture.component.html'
})
export class NewpictureComponent implements OnInit {
  link: string;
  name: string;
  linkServer: string;

  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private fileUploader: UploadFileService,
               private spinner: NgxSpinnerService) {
    this.link = 'http://it-univer43.catkov.beget.tech/uploads/images/ava.jpeg';
    this.name = '';
    this.linkServer = 'images/ava.jpeg';
  }


  async addPicture() {
   this.spinner.show();
    if (this.link !== 'http://it-univer43.catkov.beget.tech/uploads/images/ava.jpeg') {
      try {
          await this
            .rest
            .addPicture({
              link: this.linkServer,
              name: this.name
            }).then((res) => {
              console.log(res);
              this
                .data
                .addToast('Изображение успешно добавлено', '', 'success');
              this
                .router
                .navigate(['/courses/list/pictures']);
            });
      } catch (error) {
        this
          .data
          .addToast(error, '', 'error');
      }
    }
  }

  async pictureChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadVectorImage(file);

      try {
        this.link = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.linkServer = pictureUrl;
        this
          .data
          .success('Логотип добавлен');
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

  async ngOnInit() {
    await this.spinner.show();
    await this.data.getCuratorProfile();
    await this.spinner.hide();
  }
}
