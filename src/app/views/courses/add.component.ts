import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { RestApiService } from '../../rest-api.service';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ngx-toasty';
import { UploadFileService } from '../../upload-file.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { isValidDate } from 'ngx-bootstrap/timepicker/timepicker.utils';


@Component({
  templateUrl: 'add.component.html'
})
export class AddComponent implements OnInit {
  submitDisabled: boolean;
  schedule: string;
  lectorName: string;
  coursesResult: any = null;
  startDate: string;
  endDate: string;
  courseId: number;
  logo: string;
  lecturerPhoto: string;
  lecturerPhotoServer: string;
  logoServer: string;
  endApplyDate: string;
  place: string;
  selectedCourse: any = null;
  isStartDateCorrect: boolean;
  isEndDateCorrect: boolean;
  isImageCorrect: boolean;
  isApplyEndDateCorrect: boolean;
  isPlaceCorrect: boolean;
  isScheduleCorrect: boolean;
  allCorrect: boolean;
  pictureId: number;
  pictures: any = null;

  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private fileUploader: UploadFileService,
              private spinner: NgxSpinnerService) {
    this.courseId = 0;
    // this.pictureId = 1;
    // TODO: check why so
    this.place = '';
    this.schedule = '';
    this.lectorName = '';
    this.startDate = '';
    this.endDate = '';
    this.endApplyDate = '';
    this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/images/ava.jpeg';
    this.logo = 'http://it-univer43.catkov.beget.tech/uploads/images/ava.jpeg';
    this.lecturerPhotoServer = 'images/ava.jpeg';
    this.logoServer = 'images/ava.jpeg';
    this.isStartDateCorrect = true;
    this.isEndDateCorrect = true;
    this.isApplyEndDateCorrect = true;
    this.isScheduleCorrect = true;
    this.allCorrect = true;
    this.isPlaceCorrect = true;
  }

  isValidDate(s) {
    const bits = s.split('.');
    const d = new Date(+bits[2], bits[1] - 1, +bits[0]);
    return (d && (d.getMonth() + 1) === +bits[1]) && (s.length === 10);
  }

  async addCourse() {
    this.submitDisabled = true;
    this.isImageCorrect = !!this.pictureId;
    this.isEndDateCorrect = this.isValidDate(this.endDate);
    this.isStartDateCorrect = this.isValidDate(this.startDate);
    this.isApplyEndDateCorrect = this.isValidDate(this.endApplyDate);
    this.isScheduleCorrect = (this.schedule !== '');
    this.isPlaceCorrect = (this.place !== '');
    this.allCorrect = this.isImageCorrect && this.isEndDateCorrect && this.isStartDateCorrect && this.isApplyEndDateCorrect && this.isScheduleCorrect && this.isPlaceCorrect;
    if (this.allCorrect) {
      try {
        if (this.courseId !== 0 && this.startDate !== '' && this.endDate !== '' && this.endApplyDate !== '') {
          await this
            .rest
            .addCourseInstance({
              course_id: this.courseId,
              start_date: this.startDate,
              end_date: this.endDate,
              schedule: this.schedule,
              place: this.place,
              apply_end_date: this.endApplyDate,
              picture_id: this.pictureId
            }).then((res) => {
              console.log(res);
              this
                .data
                .addToast('Курс успешно добавлен', '', 'success');
              this
                .router
                .navigate(['/courses/ongoing']);
            });
        } else {
          this
            .data
            .addToast('Выберите курс!', '', 'error');
        }
      } catch (error) {
        this
          .data
          .addToast(error, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

  async selectNewCourse() {
    this.selectedCourse = await this.coursesResult.data.courses.find(course => course.id === +this.courseId);
    console.log(this.selectedCourse);
  }

  async logoChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadImage(file);

      try {
        this.logo = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.logoServer = pictureUrl;
        this
          .data
          .success('Логотип добавлен');
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

  async photoChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const pictureUrl = await this
        .fileUploader
        .uploadImage(file);
      try {
        this.lecturerPhoto = 'http://it-univer43.catkov.beget.tech/uploads/' + pictureUrl;
        this.lecturerPhotoServer = pictureUrl;
        this
          .data
          .success('Фото лектора добавлено');
      } catch (error) {
        this
          .data
          .error(error['message']);
      }
    }
  }

  async ngOnInit() {
    await this.spinner.show();
    await this.data.getCuratorProfile();
    this.pictures = await this.rest.getPictures();
    this.coursesResult = await this.rest.getUniversityCourses(this.data.user.university_id);
    await this.spinner.hide();
  }
}
