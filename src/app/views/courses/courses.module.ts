// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ListComponent } from './list.component';
import { AddComponent } from './add.component';
import { EditComponent } from './edit.component';

// Theme Routing
import { CoursesRoutingModule } from './courses-routing.module';
import {FormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {OngoingComponent} from './ongoing.component';
import {CourseComponent} from './course.component';
import {AutofocusModule} from 'angular-autofocus-fix';
import {NewpictureComponent} from './newpicture.component';
import {PicturesComponent} from './pictures.component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaskModule.forRoot(),
    CoursesRoutingModule,
    FormsModule,
    AutofocusModule
  ],
  declarations: [
    ListComponent,
    AddComponent,
    EditComponent,
    OngoingComponent,
    CourseComponent,
    NewpictureComponent,
    PicturesComponent
  ]
})
export class CoursesModule { }
