import { Component, OnInit } from '@angular/core';
import { getStyle, rgbToHex } from '@coreui/coreui/dist/js/coreui-utilities';
import {DataService} from '../../data.service';
import {RestApiService} from '../../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  templateUrl: 'pictures.component.html'
})


export class PicturesComponent implements OnInit {
  pictures: any = null;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    if (this.pictures == null) {
      await this.spinner.show();
    }

    await this.data.getCuratorProfile();
    this.pictures = await this.rest.getPictures();
    await this.spinner.hide();
  }
}

