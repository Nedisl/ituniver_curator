import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {RestApiService} from '../rest-api.service';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ngx-toasty';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'reset.component.html'
})

// TODO: Fix validation shit code to Rx driven
export class ResetComponent implements OnInit {
  first_name: string;
  last_name: string;
  father_name: string;
  email: string;
  password: string;
  repeat_password: string;
  university_id: number;
  phone: string;

  isEmailValid: boolean;
  isPasswordMatch: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isUniversityCorrect: boolean;
  allCorrect: boolean;

  submitDisabled = false;
  constructor(public data: DataService,
              private rest: RestApiService,
              private router: Router,
              private spinner: NgxSpinnerService) {
  }

  async ngOnInit() {
    this.isEmailValid = true;
    this.isPasswordMatch = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isUniversityCorrect = true;
    this.allCorrect = true;
    this.first_name = '';
    this.last_name  = '';
    this.father_name = '';
    this.email = '';
    this.password = '';
    this.repeat_password = '';
    this.university_id = 0;
    this.phone = '';
  }

  async reset() {
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid =  re.test(String(this.email).toLowerCase());

    if (this.isEmailValid) {
      try {
            await this.spinner.show();
            await this
              .rest
              .resetCurator({
                email: this.email
              }).then(async (res) => {
                await alert('Вы успешно сбросили пароль. Проверьте свою почту');
                await this.spinner.hide();
                this
                  .router
                  .navigate(['/login']);
              });
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }

      this.submitDisabled = false;
    }
  }
}
