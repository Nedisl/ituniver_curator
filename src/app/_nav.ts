export const navItems = [
  {
    name: 'Профиль',
    url: '/dashboard',
    icon: 'icon-user'
  },
  {
    title: true,
    name: 'Настройки'
  },
  {
    name: 'Безопасность',
    url: '/settings/security',
    icon: 'icon-lock'
  },
  {
    name: 'Профиль',
    url: '/settings/profile',
    icon: 'icon-user'
  },
  {
    title: true,
    name: 'Курсы'
  },
  {
    name: 'Добавить курс',
    url: '/courses/add',
    icon: 'icon-briefcase'
  },
  {
    name: 'Каталог',
    url: '/courses/catalog',
    icon: 'icon-book-open'
  },
  {
    name: 'Список',
    url: '/courses/ongoing',
    icon: 'icon-book-open'
  },
  {
    title: true,
    name: 'Галерея'
  },
  {
    name: 'Добавить',
    url: '/courses/course/pictures/add',
    icon: 'icon-briefcase'
  },
  {
    name: 'Список',
    url: '/courses/list/pictures',
    icon: 'icon-book-open'
  },
];
